import { AuthGuard } from './guards'
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  // { path: 'home', loadChildren: './home/home.module#HomePageModule' },

  { path: 'pages', loadChildren: './pages/pages.module#PagesPageModule' },
  { path: 'profile', loadChildren: './pages/profile/profile.module#ProfilePageModule' },
  { path: 'dashboard', loadChildren: './pages/dashboard/dashboard.module#DashboardPageModule' },
  { path: 'logout', loadChildren: './pages/logout/logout.module#LogoutPageModule' },
  { path: 'requestleave', loadChildren: './pages/requestleave/requestleave.module#RequestleavePageModule' },
  
  { path: 'admin',
    loadChildren:'./admin/admin.module#AdminPageModule',  
  },

  { path: 'adminprofile',
    loadChildren: './adminPage/adminprofile/adminprofile.module#AdminprofilePageModule', 
  },

  { path: 'adminstatus', 
    loadChildren: './adminPage/adminstatus/adminstatus.module#AdminstatusPageModule',
  },

  { 
    path: 'superadminprofile',
    loadChildren: './superadminpage/superadminprofile/superadminprofile.module#SuperadminprofilePageModule'
  },

  { path: 'employeesdetailspage',
    loadChildren: './pages/employeesdetailspage/employeesdetailspage.module#EmployeesdetailspagePageModule' 
  },

  { path: 'notification', 
    loadChildren: './pages/notification/notification.module#NotificationPageModule'
  },
  { path: 'settings', loadChildren: './pages/settings/settings.module#SettingsPageModule' },
  { path: 'aboutus', loadChildren: './settingspage/aboutus/aboutus.module#AboutusPageModule' },
  { path: 'privacypolicy', loadChildren: './settingspage/privacypolicy/privacypolicy.module#PrivacypolicyPageModule' },
  { path: 'terms', loadChildren: './settingspage/terms/terms.module#TermsPageModule' },
  { path: 'help', loadChildren: './settingspage/help/help.module#HelpPageModule' }
];



@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
