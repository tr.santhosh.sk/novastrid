import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../services';
import { Employee } from '../../models'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { first } from 'rxjs/operators';



@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  selectEmployees = [
    {
      title: 'MANAGERS',
      icon: 'ios-glasses',
      url: '/employeesdetailspage'
    },
    {
      title: 'ANGULAR',
      icon: 'logo-angular',
      url: '/employeesdetailspage'
    },
    {
      title: 'IOS',
      icon: 'phone-portrait',
      url: '/employeesdetailspage'
    },
    {
      title: 'IONIC',
      icon: 'ionic',
      url: '/employeesdetailspage'
    },
    {
      title: 'ANDROID',
      icon: 'logo-android',
      url: '/employeesdetailspage'
    },
    {
      title: 'TESTING',
      icon: 'bug',
      url: '/employeesdetailspage'
    },
    {
      title: 'REACT',
      icon: '',
      url: '/employeesdetailspage'
    },
    {
      title: 'NODEJS',
      icon: 'logo-nodejs',
      url: '/employeesdetailspage'
    },
    {
      title: 'UI',
      icon: 'color-fill',
      url: '/employeesdetailspage'
    },
    {
      title: 'UX',
      icon: 'cog',
      url: '/employeesdetailspage'
    },
    {
      title: 'PHP',
      icon: 'code-working',
      url: '/employeesdetailspage'
    },
    {
      title: 'ACCOUNTS',
      icon: 'calculator',
      url: '/employeesdetailspage'
    }
  ];

  currentUser: Employee;
  NovaTeams: FormGroup;
  newProfile: string;

  profile: any = [];


  constructor(private employeeService: EmployeeService, public formBuilder: FormBuilder, ) { 
  }

  ngOnInit() {

    this.employeeService.profile.subscribe(profile => {
      this.profile.push(profile);
      // console.log(this.profile)

    }
    );

    // this.employeeService.getAll()
    //   .subscribe(datails => {
      
    //     console.log(datails);

    //   });

  }


  // newUsers(details) {
  //   this.employeeService.newProfile(this.newProfile);
  // }



}
