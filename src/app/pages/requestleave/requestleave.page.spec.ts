import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestLeavePage } from './requestleave.page';

describe('RequestleavePage', () => {
  let component: RequestLeavePage;
  let fixture: ComponentFixture<RequestLeavePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestLeavePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestLeavePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
