import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Router, ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-requestleave',
  templateUrl: './requestleave.page.html',
  styleUrls: ['./requestleave.page.scss'],
})
export class RequestLeavePage implements OnInit {

  myDate:any; minDate:any;

  profile: any = [];

  selectLeaveType=[
    {
      leaveType:'Sick Leave'
    },
    {
      leaveType:'Personal Leave'
    },
    {
      leaveType:'Emergency Leave'
    },
  ]
  leaveForm: FormGroup;
  leaveType:string;
  leaveDate:string;
  leaveReason:string;
  submitted: boolean;

  constructor( private employeeService: EmployeeService,public formBuilder:FormBuilder ,
    private route: ActivatedRoute,private router: Router, ) 
  { }

  

  ngOnInit() {

    this.leaveForm = this.formBuilder.group({
      leaveType: ['', Validators.required],
      leaveDate: ['', Validators.required],
      leaveReason:['',Validators.required,  Validators.minLength(3)]
  });

    this.employeeService.profile.subscribe(profile => {
      this.profile.push(profile);
      // console.log(this.profile)
    }
    );
  }
  get f() { return this.leaveForm.controls; }

  onSubmitLeave() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.leaveForm.invalid) {
      return;
    }

  }
}

