import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { EmployeeService } from '../services';
import { Employee } from '../models';
 

@Component({
  selector: 'app-pages',
  templateUrl: './pages.page.html',
  styleUrls: ['./pages.page.scss'],
})
export class PagesPage implements OnInit {

  profile: any = [];

  constructor( private employeeService: EmployeeService  ) { 
    
   }

  ngOnInit() {
 
    this.employeeService.profile.subscribe(profile => {
      this.profile.push(profile);
      // console.log(this.profile)

    }
    );



  //   this.employeeService.getAll().pipe(first()).subscribe(users => { 
  //     // this.login = users;
  // });
  }



}
