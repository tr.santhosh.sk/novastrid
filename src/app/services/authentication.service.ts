import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Employee } from '../models';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {


  [x: string]: any;
    constructor(private http: HttpClient) { }

    
login(key : any) {

    return this.http.post<any>(`${environment.apiUrl}/login`, key )
        .pipe(map(user => {
            // login successful if there's a jwt token in the response               
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', user.Json_token);
             return user;
            }));           
        }

logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('Json_token');
    }
     
  set(key: string, data: any): void {
      try {
      localStorage.setItem(key, JSON.stringify(key));
    } catch (e)  {
      console.error('Error saving to localStorage', e);
    }
  }
}