import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../models';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import {BehaviorSubject} from 'rxjs';

@Injectable({ 
    providedIn: 'root' 
})

export class EmployeeService {
    
  goToAdmin: any;
 
    constructor(private http: HttpClient, ) { }
    
    // get data values from api--//
    public profile = new BehaviorSubject<string>('');
    // castUser = this.profile.asObservable();

    newProfile(newEmployee: string){
        this.profile.next(newEmployee); 
      }

    getAll() :Observable<any> {
        return this.http.get<any>(`${environment.apiUrl}/login`);        
    }
    // getRequest() :Observable<any>{
    //     return this.http.get<any>(`${environment.apiUrl}/send`)
    // }
}
