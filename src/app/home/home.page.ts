import { Component, OnInit, } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService, EmployeeService } from '../services';
import { first } from 'rxjs/operators';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {
   
  loginForm: FormGroup;
  submitted= false;
  loading=false;
  returnUrl:string;
  error='';
  username: string;
  password:string;
 

  constructor(public formBuilder: FormBuilder,private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,public toastController: ToastController ,public employeeService:EmployeeService )
     { }
   ngOnInit() {
    //  login validation
    this.loginForm = this.formBuilder.group({
        username: ['', Validators.required],
        password: ['', Validators.required]
    });
    this.authenticationService.logout();

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/pages';
 }
    
 getAll(){
 if(this.username == 'getAll()' && this.password == 'getAll()'){
  this.router.navigate(["/pages"]);
 }else {
   alert("Invalid credentials");
 }
}

// password hide show button 
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
}
 
  
  get f() { return this.loginForm.controls; }
   
   onSubmit() {
     
    this.submitted = true;
    console.log('welcome')

    if (this.loginForm.invalid) {
      
        return;
    }

    this.loading=true;
    console.log('hi')

    
    var key = {
      'emp_id' : this.f.username.value,
      'password' : this.f.password.value
    }

    this.authenticationService.login(key)
            .pipe(first())
            .subscribe(
                data => {
                  this.employeeService.newProfile(data);
                  console.log(data)
                  // console.log(data);
                  // console.log(data.status);
                  // console.log(data.json.response.message);  
                    this.router.navigate([this.returnUrl]);
                },
                
                error => {
                    this.error = error;
                    // alert(error)
                    this.loading = true;
                });
}
      //   async showToast() {
      //   const toast = await this.toastController.create({
      //   message:'Login Succesfully',
      //   duration: 4000,
      //   position: 'top',
      //   showCloseButton:true,
      //   closeButtonText:'ok'
      //   });
      //   toast.present();
      //  }

}


