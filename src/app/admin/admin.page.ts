import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage implements OnInit {

  public appPages = [
    // {
    //   // title: 'Profile',
    //   // url: '/adminprofile',
    //   // icon: 'person'
    // },
    {
      title: 'Notification',
      url: '/adminstatus',
      icon: 'notifications'
    },
    // {
    //   title: 'Logout',
    //   url: '/home',
    //   icon: 'log-out'
    // }
  ];

  constructor() {     
    
    this.initializeApp();
    }

    initializeApp() {
  }

  
  ngOnInit() {
  }

}
