import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-adminstatus',
  templateUrl: './adminstatus.page.html',
  styleUrls: ['./adminstatus.page.scss'],
})
export class AdminstatusPage implements OnInit {
  toastController: any;


  constructor(public atrCtrl: AlertController) { }


  async accept(){
    const alertConfirm = this.atrCtrl.create({
      // title: 'Delete Items',
      message: 'Are You Sure Accept this Leave Request?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: _data => {
            
            console.log('No clicked');
          }
        },
        {
          text: 'Accept',
          handler: () => {
            console.log('Yes clicked');
          }
        }
      ]
    });
    (await alertConfirm).present();
  }
  async reject(){
    const alertConfirm = this.atrCtrl.create({
      // title: 'Delete Items',
      
      message: 'Are You Sure Reject this Leave Request?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('No clicked');
          }
        },
        {
          text: 'Reject',
          handler: () => {
            console.log('Yes clicked');
          }
        }
      ]
    });
    (await alertConfirm).present();   
    }
  

  ngOnInit() {
  }

}
