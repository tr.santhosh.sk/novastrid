import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminstatusPage } from './adminstatus.page';

describe('AdminstatusPage', () => {
  let component: AdminstatusPage;
  let fixture: ComponentFixture<AdminstatusPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminstatusPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminstatusPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
