export class Employee {
    id: number;
    username: string;
    password: string;
    firstname:string;
    lastname:string;
    token?:string;
}