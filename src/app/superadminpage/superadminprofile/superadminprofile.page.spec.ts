import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperadminprofilePage } from './superadminprofile.page';

describe('SuperadminprofilePage', () => {
  let component: SuperadminprofilePage;
  let fixture: ComponentFixture<SuperadminprofilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperadminprofilePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperadminprofilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
