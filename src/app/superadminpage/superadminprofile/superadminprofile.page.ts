import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-superadminprofile',
  templateUrl: './superadminprofile.page.html',
  styleUrls: ['./superadminprofile.page.scss'],
})
export class SuperadminprofilePage implements OnInit {

  public appPages = [
    {
      title: 'Profile',
      url: '/adminprofile',
      icon: 'person'
    },
    {
      title: 'Notification',
      url: '/adminlogout',
      icon: 'notifications'
    },
    {
      title: 'Logout',
      url: '/home',
      icon: 'log-out'
    }
  ];

  constructor() {     
    
    this.initializeApp();
    }

    initializeApp() {
  }

  
  ngOnInit() {
  }

}
